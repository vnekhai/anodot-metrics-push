package main

import (
	"bufio"
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

type Metric struct {
	Properties struct {
		What            string `json:"what"`
		Geo             string `json:"Geo"`
		Device          string `json:"Device"`
		ProductCategory string `json:"ProductCategory"`
		TargetType      string `json:"target_type"`
	} `json:"properties"`
	Timestamp int32 `json:"timestamp"`
	Value     int   `json:"value"`
}

///

func (m Metric) randArray(start, end time.Time, size int, randValuesGenerator func() int) ([]Metric, error) {

	startUnix := start.Unix()
	endUnix := end.Unix()

	//log.Println("Start:", startUnix)
	//log.Println("End:", endUnix)

	diffValue := endUnix - startUnix
	//log.Println("Diff:", diffValue)
	if diffValue < int64(size) {
		return nil, errors.New("start time should not be > end time")
	}

	step := int(math.RoundToEven(float64(diffValue) / float64(size)))

	res := make([]Metric, 0, size)
	for i := 0; i < size; i++ {
		m.Timestamp = int32(startUnix) + int32((i+1)*step)
		m.Value = randValuesGenerator()
		res = append(res, m)
	}

	return res, nil
}

func main() {

	http.DefaultTransport.(*http.Transport).MaxIdleConnsPerHost = 100

	metricTemplate := Metric{}
	metricTemplate.Properties.What = "UsersOnline"
	metricTemplate.Properties.Geo = "VOVAN"
	metricTemplate.Properties.Device = "PS4"
	metricTemplate.Properties.ProductCategory = "Games"
	metricTemplate.Properties.TargetType = "gauge"

	now := time.Now()

	//about a year
	startTime := now.Add(time.Duration(-8400) * time.Hour)
	endTime := startTime.Add(time.Duration(3000) * time.Second)

	codes := GetCountryCodes("./countries.csv")
	gorutinesLimit := make(chan struct{}, 10)
	for {
		if endTime.After(now) {
			break
		}

		for _, v := range codes {
			metricTemplate.Properties.Geo = v
			gorutinesLimit <- struct{}{}
			log.Println(metricTemplate.Properties.Geo)

			valuesGenerator := func() int {
				return randInt(0, randInt(50, 1000))
			}
			metricTemplate.Properties.Geo = v
			metrics, err := metricTemplate.randArray(startTime, endTime, 180, valuesGenerator)
			if err != nil {
				log.Fatal(err)
			}

			go postMetrics(gorutinesLimit, metrics)
		}

		startTime = startTime.Add(time.Duration(180) * time.Minute)
		endTime = endTime.Add(time.Duration(180) * time.Minute)
	}
}

func postMetrics(ch chan struct{}, metrics []Metric) {
	metricsUrl, e := metricsUrl("http://127.0.0.1:8080", "1bc864529ad50b3750739d14213f4cd2")
	if e != nil {
		log.Fatal("Unable to generate metrics url", e)
	}

	bolB, _ := json.Marshal(metrics)
	resp, err := http.Post(metricsUrl.String(), "application/json", strings.NewReader(string(bolB)))
	if err != nil {
		log.Fatal(err)
	}

	if resp.StatusCode != 200 {
		bytes, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(bytes))
	}

	io.Copy(ioutil.Discard, resp.Body)
	resp.Body.Close()

	<-ch
}

func GetCountryCodes(filePath string) []string {

	res := make([]string, 0)
	// Load a csv file.
	f, _ := os.Open(filePath)

	// Create a new reader.
	r := csv.NewReader(bufio.NewReader(f))
	for {
		record, err := r.Read()
		// Stop at EOF.
		if err == io.EOF {
			break
		}
		// Display record.
		// ... Display record length.
		// ... Display all individual elements of the slice.

		res = append(res, record[2])
	}

	return res
}

func toArray(template Metric, start time.Time, size int) []Metric {
	now := time.Now().Unix()
	startUnix := start.Unix()

	if startUnix > now {
		panic("start should not ne in future")
	}

	diff := now - startUnix
	step := int(math.RoundToEven(float64(diff) / float64(size)))

	metrics := make([]Metric, 0, size)

	for i := 0; i < size; i++ {
		template.Timestamp = int32(startUnix) + int32((i+1)*step)
		template.Value = randInt(10, 15)
		metrics = append(metrics, template)
	}

	return metrics
}

func metricsUrl(base, token string) (url.URL, error) {

	parse, err := url.Parse(base)
	if err != nil {
		return url.URL{}, err
	}

	relative, err := url.Parse("/api/v1/metrics")
	if err != nil {
		return url.URL{}, err
	}

	queryString := relative.Query()
	queryString.Add("token", token)
	queryString.Add("protocol", "anodot20")

	relative.RawQuery = queryString.Encode()

	reference := parse.ResolveReference(relative)

	return *reference, nil
}

func randInt(min, max int) int {
	rand.Seed(time.Now().UnixNano())

	return rand.Intn(max-min) + min
}

func createAnomalies(metrics []Metric, length int) {

	halfSize := len(metrics) / 2
	if length >= halfSize {
		panic("To long anomaly")
	}

	startValue := metrics[halfSize].Value
	for i := halfSize; i <= halfSize+length; i++ {
		metrics[i].Value = startValue + randInt(-5, -3)
	}

}
