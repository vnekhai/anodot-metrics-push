package main

import (
	"crypto/sha1"
	"encoding/base64"
	"golang.org/x/crypto/pbkdf2"
)

//used to encode password which could be later saved as user password in MongoDB
/*func main() {
	saltBase64 := "FudYe0LEVQZfQGJbqvwHsg=="
	plainTextPasswordToEncode := "password"

	salt, err := base64.StdEncoding.DecodeString(saltBase64)
	if err != nil {
		log.Fatal("Unable to decode salt from base64 value.", err)
	}
	password := hashPassword(plainTextPasswordToEncode, salt)
	log.Println("Your encoded password: " + password)
}*/
func hashPassword(passwd string, salt []byte) string {
	tempPasswd := pbkdf2.Key([]byte(passwd), salt, 10000, 64, sha1.New)
	return base64.StdEncoding.EncodeToString(tempPasswd)
}


//db.users.update({email : "vova@anodot.com"}, {$set : {password :"QwdtFvYiFwuhHINGylaR4fHTFfBfbUEThN7/giA6Ci9Piujb14V9cuannMEXmSgEA0UBpb2V5ySRV6lx8H3pNQ=="}})