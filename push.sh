#!/usr/bin/env bash

set -x
curl -k \
-v -X  POST \
-d '[{"properties":{"what":"ActiveUsers","Geo":"US","Device":"PS2","ProductCategory":"Games","target_type":"gauge"},"timestamp":1557407335,"value":12}]' \
-H "Content-Type: application/json" \
'https://roman/api/v1/metrics?token=super-secret-token&protocol=anodot20'